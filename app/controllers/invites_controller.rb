class InvitesController < ApplicationController
  before_action :set_community #, except: [:edit]

  def index
    if params[:order].nil?
      order='created_at desc'
    else
      order=params[:order]
    end
    @invites=@community.invites.paginate(:page => params[:page],:per_page => 20).order(order)
    respond_to do |format|
      format.html
      format.json
      format.js
    end
  end

  def new
    @invite=@community.invites.new
  end

  def edit
    numeric =  Float(params[:id]) != nil rescue false
    if numeric
      @invitation_send=InvitationSend.find(params[:id])
    else
      @invitation_send=InvitationSend.find_by_uuid(params[:id])
    end
    @invite=@invitation_send.invite
    if @invitation_send.nil?
       redirect_to :root, notice: 'Invitación incompleta'
    else
      if @invitation_send.email==current_user.email
         @invitation_send.accepted=DateTime.now
         @invitation_send.save
         redirect_to community_url(@community.subdomain)
      else
        redirect_to :root, notice: 'Esta invitacion no es para usted'
      end
    end
  end

  def create
    @invite = @community.invites.new(invite_params)
    @invite.user_id = current_user.id
    emails=[]
    unless params[:invite][:email_list].nil?
      emails = params[:invite][:email_list].split(/\n|,|\s/)
    end

    @invite.emails=emails.size unless emails.empty?

    if @invite.save
      emails.each do |email|
        unless email.empty?
          member = nil
          if user = User.find_by_email(email)
            member = user.members.find_by_community_id(@community.id)
          end
          if member.nil?
            begin
              @invite_emails = @invite.invitation_sends.new(:email=>email)
              @invite_emails.save
            rescue Exception => e
              logger.debug e.inspect
            end
          end
        end
      end
    else
      logger.info @invites.inspect
    end
    users= @invite.invitation_sends.where("to_user_id is not null").count
    send = @invite.invitation_sends.where("to_user_id is null").count
   @invite.send_notifications = users
    @invite.send_emails = send
    @invite.save
    respond_to do |format|
      format.html {redirect_to community_invites_path(@community.subdomain)}
      format.json
      format.js
    end
  #rescue  Exception => e
  #  logger.info e.inspect
  end

  def update
    @invite=InvitationSend.where("uuid=? and accepted=null and (to_user_id=? or email=?)",params[:id],current_user.id,current_user.email).first
    if @invite
      unless params[:accepted].nil?
        @invite.accepted = DateTime.now.to_s(:db)
        @invite.to_user_id = current_user.id
        if @invite.save
          @member=@community.members.new(user_id: current_user.id)
          @member.save
        end
      end
    end
    redirect_to community_url(@community.subdomain)
  end
  private
  def set_community
    numeric =  Float(params[:community_id]) != nil rescue false
    if numeric
        @community=Community.find(params[:community_id])
    else
      @community=Community.find_by_subdomain(params[:community_id])
    end
    @owner = @community.user if @community.user.id==current_user.id
    @member = @community.get_member(current_user)
  end
  def invite_params
    params.require(:invite).permit(:email_list, :file)
  end
end
