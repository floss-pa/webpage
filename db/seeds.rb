# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

User.create!({:name=>"Happy Developer",:email => "developer@floss-pa.net", :password => "adminadmin", :password_confirmation => "adminadmin", :confirmed_at => Date.today.to_s(:db)})
Page.create!(:user_id=>1,:title=>"Sitio de la comunidad",:language=>"es", :content=> "Contentido de ejemplo",:carousel=>false, :is_publish=>true,:is_home=>true,:in_menu=>false, :keywords=>"Floss")
