class BoardMessagesController < ApplicationController
  before_action :set_community_board
  def new
    @board_message=@community.boards.find(@board.id).board_messages.new
    respond_to do |format|
        format.html
        format.json
        format.js
    end
  end

  def edit
    @board_message=BoardMessage.find(params[:id])
    respond_to do |format|
        format.html
        format.json
        format.js
    end
  end

  def show
  end

  def update
    @board_message=BoardMessage.find(params[:id])
    respond_to do |format|
      if @board_message.update(board_message_params)
        format.html
        format.json
        format.js
      else
        format.js
      end
    end
  end

  def create
    @board_message=BoardMessage.new(board_message_params)
    @board_message.board_id=@board.id
    @board_message.user_id=current_user.id
    respond_to do |format|
      if @board_message.save
        format.html
        format.json
        format.js
      else
        format.js
      end
    end
  end
  def destroy
    @board_message=BoardMessage.find(params[:id])
    @board_message.deleted=true
    respond_to do |format|
      if @board_message.save
        format.html
        format.json
        format.js
      else
        format.js
      end
    end
  end
  private

  def set_community_board
    numeric =  Float(params[:community_id]) != nil rescue false
    if numeric
      @community = Community.find(params[:community_id])
    else
      @community = Community.find_by_subdomain(params[:community_id])
    end

    @board= Board.find(params[:board_id])
    if user_signed_in?
      @member = @community.get_member(current_user)
    end
  end

  def board_message_params
    params.require(:board_message).permit(:content,:views,:pin,:deleted)
  end
end
