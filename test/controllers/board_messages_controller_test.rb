require 'test_helper'

class BoardMessagesControllerTest < ActionDispatch::IntegrationTest
  test "should get show" do
    get board_messages_show_url
    assert_response :success
  end

  test "should get update" do
    get board_messages_update_url
    assert_response :success
  end

  test "should get create" do
    get board_messages_create_url
    assert_response :success
  end

end
