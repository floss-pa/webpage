class Event < ApplicationRecord
  belongs_to :user
  belongs_to :community, optional: true
  has_many :tickets #, inverse_of: :event
  has_many :attendees, :through => :tickets
  has_many :badges, as: :badgeble
  has_many :schedules
  has_many :comments, as: :commentable
  has_many :event_organizers
  date_time_attribute :start_at
  date_time_attribute :end_at
  validates :title, presence: true, length: { minimum: 8}
  validates :title, uniqueness: true
  validates :description, presence: true, length: { minimum: 10}
  validates :start_at, presence: true
  validates :end_at, presence: true
  has_attached_file :image,
    styles: { :event=>"1200x600",:mail=> "825x300",:ticket=> "600x300", :project=>"320x150",:medium => "200x200>" } ,
     :processors => [:cropper],
     :default_style => :event,
     default_url: ->(attachment) { ActionController::Base.helpers.asset_path("event_default.png") }
  validates_attachment_content_type :image, :content_type => /^image\/(png|jpeg|jpg)/
  accepts_nested_attributes_for :tickets
  attr_accessor :crop_x, :crop_y, :crop_x2, :crop_y2, :crop_w, :crop_h, :crop_vy, :co_organizer

  after_update :reprocess_image, :if => :cropping?
  after_create :reprocess_image, :if => :cropping?
  before_save :set_subdomain
  after_create :give_badge
  after_save :set_organizers
  before_save :remove_whitspace

  accepts_nested_attributes_for :schedules, reject_if: :reject_schedule
  include Bootsy::Container
  #has_paper_trail

  def create_calendar_entry
    cal = Icalendar::Calendar.new
      cal.timezone do |t|
        t.tzid = "America/Panama"
      end
      cal.event do |e|
        e.dtstart = Icalendar::Values::DateTime.new(self.start_at)
        e.dtend = Icalendar::Values::DateTime.new(self.end_at)
        e.summary = self.title
        e.description = self.description
        e.ip_class = "PRIVATE"
      end
      cal
  end

  def name
    self.title
  end

  private

  def cropping?
    !crop_x.blank? && !crop_y.blank? && !crop_w.blank? && !crop_h.blank?
  end

  def image_geometry(style = :original)
    tmpfile = image.queued_for_write[style]
    @geometry ||= {}
    @geometry[style] ||= Paperclip::Geometry.from_file(tmpfile)
  end

  def reprocess_image
    image_geometry(:original)
    logger.info @geometry.inspect
    self.crop_h = 600
    self.crop_w = 1200
    image.assign(image)
    image.save
  end

  def reject_schedule(attributes)
    attributes['speaker_name'].blank?
    attributes['email'].blank?
    attributes['talk_name'].blank?
    attributes['date_hour'].blank?
    attributes['hour_start'].blank?
    attributes['hour_end'].blank?
  end

  def set_subdomain
    I18n.transliterate(self.title.strip).gsub(/[^0-9A-Za-z]/,'-')
  end

  def send_emails
    events = self.user.events.where("id<>?",self.id)
    events.each do |event|
      event.attendees.each do |attendee|
        EventsMailer.invites(attendee.user,self).deliver_later
      end
    end
  end

  def give_badge
    ActivityBadgesJob.perform_now('Event',self)
  end

  def remove_whitspace
    self.title = self.title.strip
  end

  def set_organizers
    unless self.co_organizer.present?
      organizers = self.co_organizer.split(',')
      organizers.each do |organizer|
        if is_email?(organizer.strip)
          user = User.find_by_email(organizer.strip)
          if user
            create_organizer(user,nil)
          else
            create_organizer(nil,organizer.strip)
          end
        else
          user = User.find_by_name(organizer.strip)
          if user
            create_organizer(user,nil)
          end
        end
      end
      remove_organizers(organizers)
    else
      #self.event_organizers.each do |organizer|
      #  organizer.update(:removed=>true)
      #ssend
    end
  end

  def create_organizer(user,email)
    unless user.nil?
      event_organizer = EventOrganizer.where(:user_id=>user.id, :event_id=>self.id).first
      unless event_organizer.nil?
        event_organizer.update(:removed=>false)
      else
        EventOrganizer.create(:user_id=>user.id, :event_id=>self.id)
      end
    else
      event_organizer = EventOrganizer.where(:event_id=>self.id,:email=>email).first
      unless event_organizer.nil?
        event_organizer.update(:removed=>false)
      else
        EventOrganizer.create(:event_id=>self.id,:email=>email)
      end
    end
  end

  def remove_organizers(organizers)
    event_organizers = EventOrganizer.where(:event_id=>self.id)
    if organizers.size < event_organizers.size
      ids = []
      organizers.each do |organizer|
        if is_email?(organizer.strip)
          user = User.find_by_email(organizer.strip)
          unless user.nil?
            org = EventOrganizer.where(:event_id=>self.id,:user_id=>user.id).first
          else
            org = EventOrganizer.where(:event_id=>self.id,:email=>organizer.strip).first
          end
          unless org.nil?
            ids << org.id
          end
        else
          user = User.find_by_name(organizer.strip)
          org = EventOrganizer.where(:event_id=>self.id,:user_id=>user.id).first
          unless org.nil?
            ids << org.id
          end
        end
      end
      EventOrganizer.where("event_id=? and id not in (?)",self.id,ids).update_all(:removed=>true)
    end
  end

  def is_email?(email)
    email =~  URI::MailTo::EMAIL_REGEXP
  end
end
