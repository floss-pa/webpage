require 'resque/tasks'
require 'resque/scheduler/tasks'
Resque.redis = 'redis://redis:6379'

task "resque:preload" => :environment

namespace :resque do

 task "resque:setup" => :environment do
   ENV['QUEUE'] = '*'
   ENV['TERM_CHILD'] = '1'
   ENV['RESQUE_TERM_TIMEOUT'] = '10'
 end

 task :setup_schedule => :setup do
    require 'resque-scheduler'
    require 'resque/scheduler/server'
    require 'active_scheduler'
    Resque::Scheduler.dynamic = true
  end

 desc "Alias for resque:work (To run workers )"
 task "jobs:work" => "resque:work"

end
