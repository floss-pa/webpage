class News < ApplicationRecord
  belongs_to :user
  has_one :news_content,inverse_of: :news
  validates :user, presence: true
  validates :title, presence: true, length: {minimum: 6, maximum: 255}, allow_nil: false
  accepts_nested_attributes_for :news_content
  before_save :set_published_date
  has_paper_trail
  after_create :give_badge
  before_save :remove_whitspace
  
  def set_published_date
    if self.publish
      self.published_at=Date.today
    end
  end
  def give_badge
    ActivityBadgesJob.perform_now('Event',self)
  end

  def remove_whitspace
    self.title = self.title.strip
  end
end
