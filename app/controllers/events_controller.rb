  class EventsController < ApplicationController
  skip_before_action :authenticate_user!, only: [:index,:show,:calendar_download]
  before_action :set_event, only: [:show, :edit, :update, :destroy, :download, :invites, :admin_invites]
  skip_authorization_check :only => [:calendar_download]
  load_and_authorize_resource
  rescue_from ActiveRecord::RecordNotFound, with: :handle_record_not_found

  # GET /events
  # GET /events.json
  def index
    by_month=nil
    by_year=nil
    event_dat=nil
    set_meta_tags title: t(:default_site_title)
    set_meta_tags description: t(:default_description)
    if params[:day].nil?
      if params[:month].nil?
        unless params[:year.nil?]
          by_year = params[:year]
        end
      else
          by_month_year = params[:year]
          by_month = "#{params[:month]}"
      end
    else
      if Date.valid_date? params[:year].to_i, params[:month].to_i, params[:day].to_i
        event_date = Date.parse("#{params[:year]}.#{params[:month]}.#{params[:day]}")
      end
    end

     unless params[:community].nil?
       @community=Community.find_by_subdomain(params[:community])
       if user_signed_in?
         @owner = @community.user if @community.user.id==current_user.id
         @member = @community.get_member(current_user)
       end
         set_meta_tags title: @community.name + t(:events)
     end

     unless event_date.nil?
      @events = Event.paginate(:page => params[:page],:per_page => 8).where("publish=true and date(start_at)=?",event_date).order('start_at Desc')
     else
       unless by_month_year.nil?
         @events= Event.paginate(:page => params[:page],:per_page=>8).where("publish=true and year(start_at)=? and month(start_at)=?",by_month_year,by_month).order('start_at desc')
       else
         unless by_year.nil?
           @events= Event.paginate(:page => params[:page],:per_page=>8).where("publish=true and year(start_at)=?",by_year).order('start_at desc')
         else

           unless @community.nil?
               @events = @community.events.paginate(:page => params[:page],:per_page =>8 ).where("publish=true ").order('start_at DESC')
           else
               @events = Event.paginate(:page => params[:page],:per_page =>8 ).where("publish=true ", params[:id]).order('start_at DESC')
           end
         end
      end
    end
  end

  # GET /events/1
  # GET /events/1.json
  def show
    set_meta_tags title: "#{@event.title} - Floss-pa Eventos"
    set_meta_tags description:  @event.description
    set_meta_tags twitter: {
                card:  "summary",
                description: @event.description,
                title: @event.title,
                creator: "@flosspa",
                image: {
                        _:       "https://floss-pa.net/images/logo.png}",
                        width:  100,
                        height: 100,
                },
                domain: "Floss-pa"
                }
    set_meta_tags og: {
                  url: "#{request.base_url + request.original_fullpath}",
                  type: "website",
                  title: "#{@event.title} Floss-pa Eventos",
                  description: @event.description,
                  site_name: "floss-pa",
                  image: "https://floss-pa.net/images/logo.png}"
                  }

    set_meta_tags  author: user_custom_path(@event.user,@event.user.name.gsub(/\s/,'-'))
    if user_signed_in?
      @attendee = @event.attendees.where(:user_id=>current_user).first
    end
    @available=false
    @ticket_ready=true
    begin
      unless @event.tickets.first.amount.nil?
        amount = @event.tickets.first.amount - @event.attendees.count
        @available=true if amount>0
      end
      if @event.tickets.empty?
        @ticket_ready= false
      else
        if @event.tickets.first.until_at.nil?
            @ticket_ready = false if @event.start_at<=DateTime.now
        else
          @ticket_ready = false if @event.tickets.first.until_at <=DateTime.now
        end
      end
    rescue

    end
    @community = Community.find(@event.community_id) unless @event.community_id.nil?
    @comment = @event.comments.new
    @organizers = @event.event_organizers.where("acepted=true and removed=false")
    if user_signed_in?
      if @event.user_id.to_i==current_user.id.to_i
        @comments = @event.comments.where("comments.deleted=false").order("id desc")
      else
        @comments = @event.comments.where("comments.deleted=false and comments.flagged=false").order("id desc")
      end
    else
      @comments = @event.comments.where("comments.deleted=false and comments.flagged=false").order("id desc")
    end
  end

  # GET /events/new
  def new
    unless params[:community].nil?
      @community = Community.find_by_subdomain(params[:community])
      @event = @community.events.new
    else
      @event = Event.new
    end
    @tickets = @event.tickets.new
    @schedules = @event.schedules.new
    @organizers = []
  end

  # GET /events/1/edit
  def edit
    @schedules = @event.schedules.empty? ? @event.schedules.new : @event.schedules
    organizers = @event.event_organizers.where("event_organizers.removed=false")
    @organizers = []
    unless organizers.empty?
      organizers.each do |organizer|
        if organizer.user_id.nil?
          @organizers << organizer.email
        else
          @organizers << organizer.user.name
        end
      end
    end
  end

  # POST /events
  # POST /events.json
  def create
    @event = current_user.events.new(event_params)
    unless params[:publish].nil?
      @event.publish=true
    end
    unless params[:event][:title].nil?
      @event.subdomain = I18n.transliterate(params[:event][:title]).gsub(/[^0-9A-Za-z]/,'-')
    end
    respond_to do |format|
      if @event.save
        format.html { redirect_to @event, notice: t(:was_successfully_created) }
        format.json { render :show, status: :created, location: @event }
      else
        format.html { render :new }
        format.json { render json: @event.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /events/1
  # PATCH/PUT /events/1.json
  def update
     @event.update(event_params)
     unless params[:event][:title].nil?
       @event.subdomain = I18n.transliterate(params[:event][:title]).gsub(/[^0-9A-Za-z]/,'-')
     end
     unless params[:publish].nil?
       @event.publish=true
     end
     unless params[:unpublish].nil?
       @event.publish=false
     end
     unless params[:send_invites].nil?
       @event.invite_emails_at=DateTime.now
     end
    respond_to do |format|
      if @event.save
        unless params[:send_invites].nil?
          logger.info 'job'
          EventInviteEmailsJob.set(queue: @event.subdomain).perform_later(current_user,@event)
        end
        format.html { redirect_to @event, notice: t(:was_successfully_updated) }
        format.json { render :show, status: :ok, location: @event }
      else
        format.html { render :edit }
        format.json { render json: @event.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /events/1
  # DELETE /events/1.json
  def destroy
    @event.destroy
    respond_to do |format|
      format.html { redirect_to events_url, notice: t(:was_successfully_destroyed) }
      format.json { head :no_content }
    end
  end

  def calendar_download
    cal = @event.create_calendar_entry
    filename = 'floss-pa-event.ics'
    send_data cal.to_ical, type: 'text/calendar', disposition: 'attachment', filename: filename
  end

  def unique
    @event = Event.find_by_subdomain(I18n.transliterate(params[:title].strip).gsub(/[^0-9A-Za-z]/,'-'))
    if @event.nil?
      render json: 'true'
    else
      check = 'false'
      if @event.id.to_i==params[:id].to_i
        check = 'true'
      end
      render json: check
    end
  end

  def invites
  end

  def admin_invites
    unless params[:uuid].nil?
      event_organizer  = EventOrganizer.where(:event_id=>@event.id,:uuid=>params[:uuid]).first
      unless event_organizer.nil?
        if event_organizer.user_id.nil?
          event_organizer.user_id = current_user.id
          event_organizer.acepted = true
        else
          if event_organizer.user_id == current_user.id
            event_organizer.acepted = true
          end
        end
        event_organizer.save
        flash[:notice] = t(:you_are_organizer)
        redirect_to @event
      else
        flash[:notice] = t(:sorry_you_are_no_invited_as_admin)
        redirect_to @event
      end
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_event
      numeric =  Float(params[:id]) != nil rescue false
      if numeric
        @event = Event.find(params[:id])
      else
          @event = Event.find_by_subdomain(params[:id])
      end
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def event_params
      params.require(:event).permit(:user_id, :title, :image, :location, :frecuency,
        :start_at_date, :start_at_time, :end_at_date,:end_at_time, :description,
        :keyworkds, :ticket_url, :community_id, :organizer, :publish, :latitude,
        :longitude, :crop_x, :crop_y, :crop_x2, :crop_y2, :crop_w, :crop_h, :crop_vy,
        :invite_emails, :co_organizer, :bootsy_image_gallery_id,
        tickets_attributes: [:id, :event_id, :ticket_type_id, :amount, :token, :until_at_date,:until_at_time],
        schedules_attributes: [:id,:user_id,:event_id,:speaker_name,:email,:speaker_resume,
                               :talk_name,:talk_resume, :room,:date_hour,:hour_start,
                               :hour_end,:workshop])
    end
    def handle_record_not_found
     redirect_to events_url, alert: t(:no_data_found, control: t(:event), info: params[:id])
    end
end
