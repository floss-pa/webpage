class Notification < ApplicationRecord
  belongs_to :user
  belongs_to :recipient, class_name: 'User'
  belongs_to :notifiable, polymorphic: true

  scope :unread, -> { where(read_at: nil) }

  def parent
    if self.notifiable_type=="Comment"
      parent = get_parent(self.notifiable_id)
    end
    return parent
  end

  def get_parent(id)
    comment = Comment.find(id)
    if comment.commentable_type == "Comment"
      get_parent(comment.commentable_id)
    else
      parent_obj = comment.commentable_type.constantize.find(comment.commentable_id)
      return parent_obj
    end
  end
end
