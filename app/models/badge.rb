class Badge < ApplicationRecord
  belongs_to :user, foreign_key: :owner_id
  has_many :user_badges
  has_many :users, through: :user_badges
  belongs_to :badgeble, polymorphic: true
  belongs_to :badge_rule
  has_attached_file :image,
              style: {openbadges: ["90x90", :png],
                      original: [:png]}
  validates_attachment :image, presence: true,
    content_type: { content_type: ["image/png","image/svg"] }, size: { in: 0..256.kilobytes }
  validate :image_dimensions, :on => :create
  before_save :remove_whitspace

  def claimable
    active=false
    if self.claim_until>=DateTime.now && self.expire>DateTime.now
      if self.badgeble_type=='Event'
        event=Event.find(self.badgeble_id)
        active = true if event.start_at.to_date==Date.today
      else
        active = true
      end
    end
    return active
  end

  def apply_rule(user_id)
    claim = false
    rule = self.badge_rule
    if rule.inmediatly
      claim = true if rule.times == rule.badgeble_type.constantize.find(rule.relation_class.to_sym => user_id).count
    else
      claim =  true unless self.badgeble.send(rule.relation_class).where(:user_id => user_id).first.nil?
    end
    return claim
  rescue
    logger.debug rule.inspect
    return false
  end


  private

  def image_dimensions
      required_width  = 90
      required_height = 90
      dimensions = Paperclip::Geometry.from_file(image.queued_for_write[:original].path)

      errors.add(:image, "El ancho de la imagen tiene que ser de #{required_width}px") unless dimensions.width.to_i == required_width.to_i
      errors.add(:image, "El alto de la image tiene que ser de #{required_height}px") unless dimensions.height.to_i == required_height.to_i
  end

  def remove_whitspace
    self.name = self.name.strip
  end
end
