class CreateEventOrganizers < ActiveRecord::Migration[5.1]
  def change
    create_table :event_organizers do |t|
      t.references :event, foreign_key: true
      t.references :user, foreign_key: true
      t.string :email,  limit: 200
      t.boolean :acepted, default: false
      t.boolean :removed, default: false
      t.string :uuid, unique: true

      t.timestamps
    end
  end
end
