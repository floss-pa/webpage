class CreateEmailLogs < ActiveRecord::Migration[5.1]
  def change
    create_table :email_logs do |t|
      t.references :user, foreign_key: true
      t.integer :recipient_id
      t.integer :emailable_id
      t.string :emailable_type,size: 50

      t.timestamps
    end
  end
end
