class Community < ApplicationRecord
  belongs_to :user
  has_many :members
  has_many :boards
  has_many :board_messages, :through => :boards
  has_many :invites
  has_many :events
  has_many :contacts
  has_paper_trail
  validates :user, presence: :true
  validates :name, presence: :true, uniqueness: true,
                             length: {minimum: 6, maximum: 255},
                             allow_nil: false

  validates :information, presence: true,  length: {minimum: 10},
                          allow_nil: false
  has_attached_file :logo, styles: { medium: "300x300>", thumb: "100x100>" },
                           default_url: "/images/:style/missing.png"

  validates_attachment_content_type :logo, content_type: /\Aimage\/.*\z/

  after_create :add_first_member
  after_create :give_badge
  before_save :remove_whitspace


  def get_member(user)
    self.members.where("user_id=? and removed=false",user.id).first
  end

  def check_if_member(user)
    self.members.where("user_id=?",user.id).first
  end

  def is_admin?(user)
    self.members.where("user_id=?",user.id).first.admin
  end

  private

  def add_first_member
    member=Member.new
    member.community_id = self.id
    member.user_id = self.user_id
    member.admin = true
    member.save
  end

  def give_badge
    ActivityBadgesJob.perform_now('Event',self)
  end

  def remove_whitspace
    self.name = self.name.strip
  end
end
