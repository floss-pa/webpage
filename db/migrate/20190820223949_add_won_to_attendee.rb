class AddWonToAttendee < ActiveRecord::Migration[5.1]
  def change
    add_column :attendees, :won, :boolean, default: false
  end
end
