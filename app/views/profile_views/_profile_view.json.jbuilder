json.extract! profile_view, :id, :name, :created_at, :updated_at
json.url profile_view_url(profile_view, format: :json)
