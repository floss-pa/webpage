class BoardsController < ApplicationController
  skip_before_action :authenticate_user!, only: [:index, :show]
  before_action :set_community
  load_and_authorize_resource

  def index
    @boards= @community.boards.where("deleted=false").paginate(:page => params[:page],:per_page =>20 ).order("pin desc,created_at desc")
    respond_to do |format|
      unless @boards.nil?
        format.html
        format.json
        format.js
      else
        unless @member
          notice = t(:no_member)
        else
          notice = t(:other)
        end
        format.html {redirect_to communities_url(@community.subdomain), notice: notice}
      end
    end
  end

  def new
    @board=@community.boards.new
    @board.board_messages.new
  end

  def create
    @board=@community.boards.new(board_params)
    @board.user_id=current_user.id
    @user=current_user

    respond_to do |format|
      if @board.save
        format.html
        format.json
        format.js
      else
        format.js
      end
    end
  end

  def edit
    @board=@community.boards.find(params[:id])
  end

  def update
    @board=@community.boards.find(params[:id])
    respond_to do |format|
      if @board.update(board_params)
        format.html
        format.json
        format.js
      else
        format.js
      end
    end
  end

  def destroy
      @board=@community.boards.find(params[:id])
      @board.deleted=true
      respond_to do |format|
        if @board.save
          format.html
          format.json
          format.js
        else
          format.js
        end
      end
  end

  def show
    @board=@community.boards.find(params[:id])
    if user_signed_in?
      unless @board.user_id==current_user.id
        @board.views+=1 unless @board.views.nil?
        @board.views=1 if @board.views.nil?
      end
      @board.save
    end
    @board_messages=@board.board_messages.where("deleted=false").paginate(:page => params[:page],:per_page =>20 ).order("created_at desc")
    respond_to do |format|
      format.html
      format.json
      format.js
    end
  end

  private

  def set_community
    numeric =  Float(params[:community_id]) != nil rescue false
    if numeric
      @community = Community.find(params[:community_id])
    else
      @community = Community.find_by_subdomain(params[:community_id])
    end
    if user_signed_in?
     @owner = @community.user if @community.user.id==current_user.id
     @member = @community.get_member(current_user)
   end
  end

  def board_params
    params.require(:board).permit(:subject,:views,:pin,:deleted,
                                   board_messages_attributes: [:content])
  end
end
