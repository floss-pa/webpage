class AddBadgeblesToBadge < ActiveRecord::Migration[5.1]
  def change
   add_column :badges,:badgeble_id, :bigint
   add_column :badges,:badgeble_type, :string, limit: 50
   add_index :badges, [:badgeble_id,:badgeble_type]
  end
end
