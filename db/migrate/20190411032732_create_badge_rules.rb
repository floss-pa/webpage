class CreateBadgeRules < ActiveRecord::Migration[5.1]
  def change
    create_table :badge_rules do |t|
      t.string :name
      t.integer :times
      t.boolean :inmediatly
      t.string :badgeble_type, limit: 50
      t.string :relation_class, limit: 50
      t.string :deliver, limit: 50

      t.timestamps
    end
  end
end
