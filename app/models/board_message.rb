class BoardMessage < ApplicationRecord
  belongs_to :board
  #belongs_to :user
  validates_presence_of :board
  #validates_presence_of :user
  before_save :add_user
  after_create :create_notification
  after_create :send_emails

  private

  def add_user
    logger.info self.user_id
    self.user_id=self.board.user_id if self.user_id.nil?
  end
  def recipients
    self.board.community.members
  end
  def create_notification
    recipients.each do |recipient|
      unless recipient.user.id == self.user_id
        Notification.create(recipient: recipient.user, user: User.find(self.user_id), notifiable: self, action: 'message')
      end
    end
  end
  def send_emails
    recipients.each do |recipient|
      unless recipient.user.id == self.user_id
        if recipient.user.preference.nil?
         BoardMessageMailer.new_message(recipient.user,self).deliver_later
        else
          if recipient.user.preference.email_board_notifications
            BoardMessageMailer.new_message(recipient.user,self).deliver_later
          end
        end
      end
    end
  end
end
