json.extract! oportunity, :id, :name, :created_at, :updated_at
json.url oportunity_url(oportunity, format: :json)
