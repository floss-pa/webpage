class EventInviteEmailsJob < ApplicationJob
  queue_as :default

  def perform(user,invite_to_event)
    events = user.events.where("events.id<>?",invite_to_event.id)
    events.each do |event|
      event.attendees.each do |attendee|
        to_user=attendee.user
        unless to_user.id.to_i==user.id.to_i
          if attendee.user.preference.nil?
            if EmailLog.where("recipient_id=? and emailable_id=? and emailable_type=?",attendee.user.id,invite_to_event.id,'Event').first.nil?
              EventsMailer.invites(attendee.user,invite_to_event).deliver_later
              EmailLog.create!(:user_id=>user.id,:recipient_id=>attendee.user.id,:emailable_id=>invite_to_event.id,:emailable_type=>'Event')
            end
          else
            if attendee.user.preference.email_event_invites
              if EmailLog.where("recipient_id=? and emailable_id=? and emailable_type=?",attendee.user.id,invite_to_event.id,'Event').first.nil?
                EventsMailer.invites(attendee.user,invite_to_event).deliver_later
                EmailLog.create!(:user_id=>user.id,:recipient_id=>attendee.user.id,:emailable_id=>invite_to_event.id,:emailable_type=>'Event')
              end
            end
          end
        end
      end
    end
  end
end
