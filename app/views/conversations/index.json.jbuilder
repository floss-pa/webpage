#json.array! @conversations.as_json(:include=>{:messages=>{:only=>[:user_id,:recipient_id,:readed]}}) #, partial: 'messages/message', as: :message
json.array! @conversations.joins(:messages).where("messages.readed is null and messages.user_id<>?",current_user.id).select("conversations.id,messages.id,messages.user_id")
