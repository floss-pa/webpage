FROM ruby:2.6.5

LABEL maintainer="Kiara Navarro <sophiekovalevsky@fedoraproject.org>"
LABEL version="1.0"

RUN apt-get update -qq && apt-get install -y \
            git \
            imagemagick \
            nodejs \
            make

RUN addgroup --gid 1000 developer && \
    adduser --uid 1000 --ingroup developer --home /home/developer --shell /bin/bash --disabled-password --gecos "" developer

USER developer
RUN mkdir -p /home/developer/app
WORKDIR /home/developer/app

COPY Gemfile Gemfile.lock ./

USER root
RUN chown -R developer:developer ./

USER developer

RUN bundle install

COPY . /home/developer/app
