class ContactsController < ApplicationController
  before_action :set_community
  def index
  end

  def show
  end

  def new
    @contact = @community.contacts.new
  end

  def create
    @contact = @community.contacts.new(contact_params)
    @contact.user_id=current_user.id
    respond_to do |format|
      if @contact.save
          format.html { redirect_to community_contacts_url(@community.subdomain), notice: t(:all_email_has_been_send) }
      else
        render :new
      end
    end
  end

  def preview
    @user = current_user
    @message = params[:message]
    logger.info @message
    render :file => 'contact_mailer/members.html.erb', :layout => 'mailer'
  end

  private

  def contact_params
    params.require(:contact).permit(:reply, :members,:subject,:message,:publish_to_board)
  end

  def set_community
    numeric =  Float(params[:community_id]) != nil rescue false
    if numeric
      @community = Community.find(params[:community_id])
    else
      @community = Community.find_by_subdomain(params[:community_id])
    end
    @owner = @community.user if @community.user.id==current_user.id
    @member = @community.get_member(current_user)
  end
end
