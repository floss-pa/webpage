class AddValidatedToAttendee < ActiveRecord::Migration[5.1]
  def change
    add_column :attendees, :validated, :boolean, default: false
    add_column :attendees, :validated_at, :datetime
  end
end
