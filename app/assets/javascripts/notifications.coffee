$(document).on 'turbolinks:load', ->
  if $("body").data("signed-in") == true
    setTimeout (->
      notifications()
      ), 1000
    setInterval (->
      notifications()
      ), 300000
  notifications = ->
    $.ajax
      url: '/notifications.json'
      type: 'get'
      data: ''
      success: (data) ->
        on_success(data)
      error: (data) ->
  on_success = (data) ->
    count_noti = $.map data, (notification)->
      notification.id
    items = $.map data, (notification) ->
      if $('#notification_' + notification.id).length==0
        noti = notification.template
      noti
    if count_noti.length>0
      $('#notifications_empty').remove()
    else
      $("#notification-items").html('<p class="text-center small" id="notifications_empty">No hay Notificaciones</p>')
    $("#unread-count").text(count_noti.length)
    $("#first-notification-item").after(items)
  $('#all_unread').click ->
    $('input[type=checkbox]').prop 'checked', $(this).prop('checked')
    return
