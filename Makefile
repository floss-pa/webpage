NAME = flosspa
MAIN_DATABASE = db
APP_INSTANCE = app

databases.sql.server:
	docker exec -it $(NAME)_$(MAIN_DATABASE) mysql -uroot -p

databases.sql.shell:
	docker exec -it $(NAME)_$(MAIN_DATABASE) /bin/bash

application.shell:
	docker exec -it $(NAME)_$(APP_INSTANCE) /bin/bash

application.ip:
	docker inspect -f "{{ .NetworkSettings.Networks.flosspawebpage_flosspa.IPAddress }}" flosspa_app
