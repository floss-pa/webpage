json.array! @messages do |message|
  json.id message.id
  json.body message.body
  json.template render partial: message, formats: [:html]
end
