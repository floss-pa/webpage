class CreateMessages < ActiveRecord::Migration[5.1]
  def change
    create_table :messages do |t|
      t.references :user, foreign_key: true
      t.references :conversation, foreign_key: true
      t.string :subject
      t.text :body
      t.boolean :readed

      t.timestamps
    end
  end
end
