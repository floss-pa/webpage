class AddBageRulesReferencetoBadges < ActiveRecord::Migration[5.1]
  def change
   add_reference :badges, :badge_rule, index: true
  end
end
