class DailyNotificationJob < ApplicationJob
  queue_as :default

  def perform(*args)
    Notification.unread.group("recipient_id").count.each do |user,count|
     to_user=User.find(user)
      if to_user.preference.nil?
        NotificationMailer.resumen(to_user,count).deliver_later
      else
        if to_user.preference.email_notifications
          NotificationMailer.resumen(to_user,count).deliver_later
        end
      end
    end
  end
end
