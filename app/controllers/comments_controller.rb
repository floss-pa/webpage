class CommentsController < ApplicationController
  before_action :set_commentable, only: [:show, :edit, :update, :destroy, :new, :create]
  invisible_captcha  only: [:create], on_spam: :on_spam_redirect
  def index
    @comments=@commentable.comments
  end

  def new
    @comment = @commentable.comments.new
  end

  def create
    @new= false
    @comment= Comment.where(:commentable_id=>@commentable.id,:commentable_type=>@commentable.class.name.capitalize,:user_id=>current_user.id,:content=>params[:comment][:content]).first
    if @comment.nil?
      @comment = @commentable.comments.new(comment_params)
      @comment.user_id = current_user.id
      @new=true
    end
    respond_to do |format|
      if @comment.save
        format.html { redirect_to @comment, notice: t(:was_successfully_created) }
        format.json { render :show, status: :created, location: @comment }
        format.js
      else
        format.json { render json: @comment.errors, status: :unprocessable_entity }
        format.js
      end
    end
  end

  def update
    if @comment.flagged
      @comment.flagged=false
      @comment.flagged_by=current_user.id
    else
      @comment.flagged=true
    end
    respond_to do |format|
      if @comment.save
        format.html { redirect_to @event, notice: t(:was_successfully_created) }
        format.json { render :show, status: :created, location: @comment }
        format.js
      else
        format.json { render json: @comment.errors, status: :unprocessable_entity }
        format.js
      end
    end
  end

  def destroy
    @comment.deleted=true
    respond_to do |format|
      if @comment.save
        format.html { redirect_to @event, notice: t(:was_successfully_created) }
        format.json { render :show, status: :created, location: @comment }
        format.js
      else
        format.json { render json: @comment.errors, status: :unprocessable_entity }
        format.js
      end
    end
  end

  private

  def set_commentable
    #@commentable = resource.singularize.classify.constantize.find()

   unless params[:event_id].nil?
     @commentable = Event.find(params[:event_id])
   end
   unless params[:comment_id].nil?
     @commentable = Comment.find(params[:comment_id])
   end
   unless params[:id].nil?
     @comment = Comment.find(params[:id])
   end
  end

  def comment_params
    params.require(:comment).permit(:content)
  end
  def on_spam_redirect
    redirect_to @event
  end
end
