class EventOrganizer < ApplicationRecord
  belongs_to :event
  belongs_to :user, optional: true
  before_create :set_uuid
  after_create :create_notification
  after_create :send_email

  private

  def set_uuid
    self.uuid = SecureRandom.uuid if self.uuid.nil?
  end

  def create_notification
    unless self.user_id.nil?
      Notification.create(recipient: self.user, user: self.event.user,
                          notifiable: self, action: 'admin_invite')
    end
  end

  def send_email
      EventsMailer.admin_invite(self).deliver_now
  end
end
