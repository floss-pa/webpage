class RegistrationsController < Devise::RegistrationsController
  invisible_captcha  only: [:create], on_spam: :on_spam_redirect

  private
    def on_spam_redirect
      redirect_to root_url
    end
end
