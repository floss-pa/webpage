Resque.redis = 'redis:6379'

require 'redis'

## Added rescue condition if Redis connection is failed
begin
  $redis = Redis.new(:host => Rails.configuration.redis_host, :port => 6379)
rescue Exception => e
  puts "NOT ABLE TO CONNECT"
  puts e

end

Resque.schedule = YAML.load_file Rails.root.join('config', 'schedule.yml')
