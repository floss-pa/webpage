class AddSubdomainToCommunities < ActiveRecord::Migration[5.1]
  def change
    add_column :communities, :subdomain, :string, limit: 50
    add_index :communities, :subdomain, unique: true
  end
end
