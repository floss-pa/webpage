class FeederJob < ApplicationJob
  queue_as :default

  def perform(feed)
    if feed.status
      post = RestClient.get(feed.url)
      content = Feedjira.parse post.body
      content.entries.each do |entry|
        local_entry = feed.entries.where(title: entry.title).first_or_initialize
        if entry.content.nil?
          if entry.summary.nil?
            content=nil
          else
            content=entry.summary
          end
        else
          content=entry.content
        end
        unless content.nil?
          local_entry.update_attributes(content: content.gsub('<a ','<a target="_blank"').gsub('<img ','<img class="img-responsive" '), author: entry.author, url: entry.url, published: entry.published)
          p "Synced Entry - #{entry.title}"
        end
      end
    end
  end
end
