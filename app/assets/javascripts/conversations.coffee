# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/
$(document).on 'turbolinks:load',->
  $('body').on 'click', '#conversation_id', ->
    conversation_id = $('#conversation_id').val()
    window.location = '/conversations/'+conversation_id+'/messages'
