class BadgesController < ApplicationController
  skip_before_action :authenticate_user!, only: [:index,:show]
  before_action :set_badge, only: [:show, :edit, :update, :destroy]
  load_and_authorize_resource

  # Claim badges will get the user a badges
  # GET /badges/claim/id
  def qrcode
    @badge= Badge.find_by_uuid(params[:uuid])
    unless @badge.nil?
      @qr = RQRCode::QRCode.new( badge_claim_url(@badge.uuid), :size => 8, :level => :h )
    else
      redirect_to badges_url, notice: t(:badge_not_found)
    end
  rescue RQRCode::QRCodeRunTimeError => e
    logger.info "Manage qr error show #{e.inspect}"
  end

  def claim
    unless params[:uuid].nil?
      @badge = Badge.find_by_uuid(params[:uuid])
      unless @badge.nil?
        unless current_user.user_badges.where(badge_id: @badge.id).first.nil?
          redirect_to user_custom_url(current_user.id,current_user.name),
                notice: t(:you_have_already_clame_this_badge,
                badge_name: @badge.name)
        else
          if @badge.claim_until >= Date.today && @badge.expire > Date.today
            if  @badge.apply_rule(current_user.id)
              @user_badge = current_user.user_badges.new(badge_id: @badge.id)
              if @user_badge.save
                redirect_to user_custom_url(current_user.id,current_user.name),
                    notice: t(:congratulations_you_have_earn_a_new_badge,user_name: current_user.name,badge_name: @badge.name)
              end
            else
              redirect_to badges_url,notice: t(:badge_claim_rule_fail,name: @badge.badgeble.name)
            end
          else
            redirect_to badges_url,notice: t(:badge_claim_expiration_has_been_reach)
          end
        end
      else
        redirect_to badges_url, notice: t(:badge_not_found)
      end
    end
  rescue Exception => e
    logger.debug e.message
    redirect_to badges_url, notice: t(:something_when_wrong)
  end

  def assertion
    user_badge = UserBadge.find(params[:id])
    assertion = OpenBadges::Assertion.new
    # You'd probably fill in these values from some `badges` table and
    # user information
    assertion.email = user_badge.user.email
    assertion.issued_on = Time.now
    assertion.name = user_badge.badge.name
    assertion.description = user_badge.badge.description
    assertion.badge_url = "https://floss-pa.net/badges/1"
    assertion.criteria_url = "https://floss-pa.net/badges/1"

    render json: assertion
  end
  # GET /badges
  # GET /badges.json
  def index
    if params[:event_id].nil?
      @badges = Badge.paginate(:page => params[:page],:per_page => 15).order("id desc")
      @user = UserBadge.last.nil? ? nil : UserBadge.last.user
      @badge = UserBadge.last.badge
    else
      @event = Event.find(params[:event_id])
      @badges = @event.badges.paginate(:page => params[:page],:per_page => 15).order("id desc")
    end
  end

  # GET /badges/1
  # GET /badges/1.json
  def show
    @users = @badge.users.paginate(:page => params[:page],:per_page => 24).order("id desc")
  end

  # GET /badges/new
  def new
    @badge = Badge.new
    @badgeble = nil
    unless params[:event_id].nil?
      @badgeble = Event.find(params[:event_id])
      @badge.claim_until = @badgeble.start_at.next_day
      @badge.expire = @badgeble.end_at.next_day
    end
  end

  # GET /badges/1/edit
  def edit
    @badgeble = @badge.badgeble
    unless @badgeble.nil?
      @badge.claim_until = @badgeble.start_at.next_day
      @badge.expire = @badgeble.end_at.next_day
    end
  end

  # POST /badges
  # POST /badges.json
  def create
    @badge = Badge.new(badge_params)
    @badge.owner_id = current_user.id
    @badge.uuid = SecureRandom.uuid
    unless params[:badgeble_id].nil?
      @badge.badgeble_id = params[:badgeble_id]
      @badge.badgeble_type = params[:badgeble_type]
      @badgeble = params[:badgeble_type].constantize.find(params[:badgeble_id])
    end
    respond_to do |format|
      if @badge.save
        format.html { redirect_to @badge, notice: t(:was_successfully_created) }
        format.json { render :show, status: :created, location: @badge }
      else
        format.html { render :new }
        format.json { render json: @badge.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /badges/1
  # PATCH/PUT /badges/1.json
  def update
    respond_to do |format|
      if @badge.update(badge_params)
        format.html { redirect_to @badge, notice: t(:was_successfully_updated) }
        format.json { render :show, status: :ok, location: @badge }
      else
        format.html { render :edit }
        format.json { render json: @badge.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /badges/1
  # DELETE /badges/1.json
  def destroy
    @badge.destroy
    respond_to do |format|
      format.html { redirect_to badges_url, notice: t(:was_successfully_destroyed) }
      format.json { head :no_content }
    end
  end

  private

    # Use callbacks to share common setup or constraints between actions.
    def set_badge
      @badge = Badge.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def badge_params
      params.require(:badge).permit(:owner_id, :name, :description, :criteria, :expire, :tags, :claim_until,:image,:badge_rule_id)
    end
end
