class AddUntilAtToTicket < ActiveRecord::Migration[5.1]
  def change
    add_column :tickets, :until_at, :datetime
  end
end
