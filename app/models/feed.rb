class Feed < ApplicationRecord
  belongs_to :user
  has_many :entries
  after_save :load_feed
  after_create :give_badge

  private
  def load_feed
    FeederJob.perform_now(self)
  end
  def give_badge
    ActivityBadgesJob.perform_now('Event',self)
  end
end
