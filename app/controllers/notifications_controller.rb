class NotificationsController < ApplicationController
  before_action :set_notifications

  def index
    @notification=Notification.where(recipient_id: current_user.id).unread.last
    respond_to do |format|
      format.html
      format.json
    end
  end

  def update
    unless params[:ids].nil?
      ids=params[:ids]
      ids.each do |id|
        notification=Notification.find(id)
        notification.read_at=DateTime.now
        notification.save
      end
      notice_notification = t(:updated_your_notifications)
    end
    unless params[:all_unread].nil?
      @notifications.each do |notification|
        notification.read_at=DateTime.now
        notification.save
      end
      notice_notification = t(:all_notifications_updated)
    end
    redirect_to notifications_path, notice: notice_notification
  end
  private

  def set_notifications
    @notifications = Notification.where(recipient: current_user).unread
  end
end
