# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/
# added Events Map Marker
$(document).on 'turbolinks:load', ->
  user_list = []
  $('#event_keyworkds').tokenfield()
  $('#event_co_organizer').tokenfield({autocomplete: { source: '/users/search.json',delay: 100}, showAutocompleOnFocus: true})
  $('#event_title').change ->
    check_unique_title()
  $('#event_start_at_date').change ->
    $('#event_tickets_attributes_0_until_at_date').val($('#event_start_at_date').val())
  $('#event_start_at_time').change ->
    $('#event_tickets_attributes_0_until_at_time').val($('#event_start_at_time').val())
  $('#event_tickets_attributes_0_amount').change ->
    if($('#event_tickets_attributes_0_amount').val()>0)
      $('#event_ticket_url').prop('disabled', true)
    else
      $('#event_ticket_url').prop('disabled', false)
  event_latitude = $('#event_latitude').val() || "8.984062"
  event_longitude = $('#event_longitude').val() || "-79.521593"
  if ($('#map').get(0) && !$('#show_map').get(0))
   EventMarker = L.marker([
     event_latitude
     event_longitude
   ],
     title: 'Mi Evento'
     alt: 'Floss-pa Event'
     draggable: true).addTo(map).on('dragend', ->
     coord = String(EventMarker.getLatLng()).split(',')
     lat = coord[0].split('(')
     $('#event_latitude').val lat[1]
     lng = coord[1].split(')')
     $('#event_longitude').val lng[0]
     EventMarker.bindPopup 'Moved to: ' + lat[1] + ', ' + lng[0] + '.'
     map.panTo new (L.LatLng)(lat[1], lng[0])
     update_event_location = 'https://nominatim.openstreetmap.org/search?format=json&q=' + lat[1] + ',' + lng[0]
     $.getJSON update_event_location, (info) ->
       $('#event_location').val info[0].display_name
       return
     return
    )
   $('#event_location').change ->
    event_location = 'https://nominatim.openstreetmap.org/search?format=json&q=\'' + $('#event_location').val() + '\''
    $.getJSON event_location, (data) ->
      $('#event_latitude').val data[0].lat
      $('#event_longitude').val data[0].lon
      $('#event_location').val data[0].display_name
      newLatLon = new (L.LatLng)(data[0].lat, data[0].lon)
      EventMarker.setLatLng newLatLon
      map.panTo newLatLon
      return
    return
  load_image_preview('event_image','Preview','image',600,1200)
  check_unique_title = ->
    item = $('#event_title')
    $.ajax
      url: 'unique.json'
      type: 'post'
      data: {title: $('#event_title').val()}
      success: (data) ->
        parent_item = $('#event_title').closest('.form-group')
        if(data==true)
          parent_item.removeClass('has-danger')
          parent_item.addClass('has-success')
          item.removeClass('is-invalid')
          item.addClass('is-valid')
        else
          parent_item.removeClass('has-success')
          parent_item.addClass('has-danger')
          item.removeClass('is-valid')
          item.addClass('is-invalid')
          alert('Ya existe un evento con el mismo titulo')
          item.focus()
      error: (data) ->
        console.log(data)
  search_user = ->
    search = $('#event_co_organizer-tokenfield').val()
    if(search.length>=3)
      $.ajax
        url: '/users/search.json'
        type: 'post'
        data: {q: search}
        result = []
        success: (data) ->
          for d in data
             result << d.name
          user_list = result
          console.log(data)
        error: (data) ->
          console.log(data)
