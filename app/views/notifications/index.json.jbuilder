json.array! @notifications do |notification|
  json.id notification.id
  json.template render partial: "notifications/#{notification.notifiable_type.underscore.pluralize}/#{notification.action.downcase}", locals: {notification: notification, formated: false}, formats: [:html]
end
