class ContactMailer < ApplicationMailer
  default from: 'admin <do-not-reply@floss-pa.net>'

  def members(user,community,contact)
    @user = user
    @community = community
    @contact = contact
    @message = contact.message
    if contact.reply
      @from = contact.user.email
    else
      @from =  'do-not-reply@floss-pa.net'
    end
    mail(from: @from,to:  "#{@user.name} <#{@user.email}>", subject: @contact.subject, message: @message )
  end
end
