class Comment < ApplicationRecord
  belongs_to :user
  belongs_to :commentable, polymorphic: true
  has_many :comments, as: :commentable
  after_create :create_notification
  after_save :add_notification

  private

  def recipients
    [self.commentable]
  end

  def create_notification
    recipients.each do |recipient|
      unless recipient.user.id == self.user_id
        Notification.create(recipient: recipient.user, user: User.find(self.user_id), notifiable: self.commentable, action: 'comment')
      end
    end
  end
  def add_notification
    if self.flagged
      recipients.each do |recipient|
        unless recipient.user.id == self.user_id
          Notification.create(recipient: recipient.user, user: User.find(self.user_id), notifiable: self.commentable, action: 'flagged')
        end
      end
    end
  end
end
