class UpdateFeedJob < ApplicationJob
  queue_as :default

    def perform(none)
      feeds=Feed.where(:status=>true)
      feeds.each do |feed|
        FeederJob.perform_later(feed)
      end
    end

end
