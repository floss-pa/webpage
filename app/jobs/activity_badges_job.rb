class ActivityBadgesJob < ApplicationJob
  queue_as :default

  def perform(badgable,activity)
    badge_rule=BadgeRule.where("badgeble_type=? and relation_class='user_id'",badgable)
    badge_rule.each do |rule|
      badge=Badge.find_by_badge_rule_id(rule.id)
      unless badge.nil?
        count=rule.badgeble_type.constantize.where(:user_id=>activity.user_id).count
        if rule.times.to_i==count.to_i
          UserBadge.create(:user_id=>activity.user_id,:badge_id=>badge.id)
        end
      end
    end
  end
end
