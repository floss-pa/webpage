class EventsMailer < ApplicationMailer
  default from: 'events floss-pa <do-not-reply@floss-pa.net>'
  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.events_mailer.invites.subject
  #
  def invites(user,event)
    @user  = user
    @event = event
    mail(to: "#{@user.name} <#{@user.email}>", subject: t(:attendees_invitation,
                                 user_name: @event.user.name.capitalize,
                                 event_name: @event.title) )
  end

  def comment(user,comment)
    @user    = user
    @comment = comment
    mail(from: "#{comment.user.name} <do-no-reply@floss-pa.net>",
         to: "#{@user.name} <#{@user.email}>",
         subject: "#{comment.user.name} #{t(:leave_a_comment_at)} #{@comment.event.title} " )
  end

  def admin_invite(event_organizer)
    unless event_organizer.user_id.nil?
      @user  = event_organizer.user
      send_to = "#{@user.name} <#{@user.email}>"
    else
      send_to = event_organizer.email
    end
    @event = event_organizer.event
    @event_organizer = event_organizer
    mail(from: "#{@event.user.name} <do-no-reply@floss-pa.net>",
         to: send_to,
         subject: "#{t(:you_have_been_invited_to_become_admin_of_event)} #{@event.title}")
  end
end
