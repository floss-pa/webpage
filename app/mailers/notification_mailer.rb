class NotificationMailer < ApplicationMailer
  default from: 'floss-pa notification <do-not-reply@floss-pa.net>'

    def resumen(user,notifications_count)
      @user = user
      @notifications = notifications_count
      mail(to:"#{@user.name} <#{@user.email}>", subject: t(:you_have_notifications,
           user_name: @user.name, notifications: notifications_count))
    end
end
