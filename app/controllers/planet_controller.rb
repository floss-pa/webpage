class PlanetController < ApplicationController
  skip_before_action :authenticate_user!, only: [:index,:show]
  before_action :set_feed, only: :show

  def index
    @entries = Entry.joins(:feed).where("feeds.status=true").paginate(:page => params[:page],:per_page =>10 ).order('published desc')
  end

  def show
    @entry = Entry.find(params[:id])
  end

  private
  def set_feed
    @feed = Feed.find(params[:id])
  end
end
