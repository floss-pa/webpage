class BadgeMailer < ApplicationMailer
default from: 'admin <do-not-reply@floss-pa.net>'

  def new_badge(user,badge)
    @user = user
    @badge = badge
    mail(to: "#{@user.name} <#{@user.email}>", subject: t(:congratulations_you_have_earn_a_new_badge,
         user_name: @user.name, badge_name: @badge.name))
  end
end
