namespace :daily_notification do
  desc "Notification daily"

  def log(msg)
    puts msg
    Rails.logger.info(msg)
  end

  task :send => :environment do 
    DailyNotificationJob.perform_now('do')
  end
end
