class MembersController < ApplicationController
  skip_before_action :authenticate_user!, only: [:index, :show]
  before_action :set_community
  def index
    if params[:order].nil?
      order='created_at desc'
    else
      order=params[:order]
    end
    if is_admin?
      @members=@community.members.paginate(:page => params[:page],:per_page => 20).order(order)
    else
      @members=@community.members.where("removed=false").paginate(:page => params[:page],:per_page => 20).order(order)
    end
    respond_to do |format|
      format.html
      format.json
      format.js
    end
  end
  def create
    @member = @community.check_if_member(current_user)
    if @member.nil?
      @member=@community.members.new(user_id: current_user.id)
    else
      @member.removed = false if @member.removed
    end
    respond_to do |format|
      if  @member.save
        format.html { redirect_to community_url(@community.subdomain),
          notice: t(:you_have_added_to_this_group)}
        format.json {render :show, status: :created, location: @community}
        format.js
      else
        format.html { redirect_to community_url(@community.subdomain),
          notice: t(:you_were_not_added)}
        format.js
      end
    end
  end

  def update
    if is_admin?
      @member=Member.find(params[:id])
      unless @community.user_id==@member.user_id
        if params[:remove]
          @member.removed=true
          @member.removed_by=current_user.id
        else
          @member.removed=false
        end
        unless params[:admin].nil?
          @member.admin=params[:admin]
        end
        unless params[:ban].nil?
          if params[:ban]
            @member.ban=true
            @member.ban_by=current_user.id
          else
            @member.ban=false
          end
        end
      end
    end
    respond_to do |format|
      if @member.save
        format.html { redirect_to community_url(@community.subdomain),
          notice: t(:removed)}
        format.js
      end
    end
  end

  def destroy
     @member=@community.members.find_by_user_id(current_user.id)
     @member.removed=true
     if @member.save
       redirect_to @community
     end
  end

  private

  def set_community
    numeric =  Float(params[:community_id]) != nil rescue false
    if numeric
      @community = Community.find(params[:community_id])
    else
      @community = Community.find_by_subdomain(params[:community_id])
    end
    if user_signed_in?
      @owner = @community.user if @community.user.id==current_user.id
      @member = @community.get_member(current_user)
    end
  end

  def is_admin?
    if user_signed_in?
      if @member
        @community.members.where(user_id: current_user.id).first.admin
      end
    end
  end
end
