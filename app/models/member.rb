class Member < ApplicationRecord
  belongs_to :community
  belongs_to :user

  validates :community, uniqueness: { scope: :user }

  scope :active, -> {where("removed=false")}
  after_create :create_notification

  private

  def recipients
    self.community.members.where("admin=true")
  end
  def create_notification
    recipients.each do |recipient|
      Notification.create(recipient: recipient.user, user: user, notifiable: self, action: 'Join')
    end
  end
end
