class TicketMailer < ApplicationMailer
  default from: 'tickets <do-not-reply@floss-pa.net>'

  def new_ticket(attendee,ical)
    @attendee = attendee
    @user = @attendee.user
    @event = @attendee.event
    begin
      @qr = RQRCode::QRCode.new( attendee_url(@attendee.id), :size => 5, :level => :h )
    rescue RQRCode::QRCodeRunTimeError => e
      logger.info "Manage qr error in new ticket #{e.inspect}"
    end
    attachments['floss-pa-event.ics'] = { mime_type: 'application/ics', content: ical } unless ical.nil?
    mail(from: "#{@event.title} - ticket <do-not-replay@floss-pa.net>",to: "#{@user.name} <#{@user.email}>", subject: t(:ticket_for)  + ' ' + @event.title)
  end
end
