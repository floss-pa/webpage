class InviteMailer < ApplicationMailer
default from: 'admin <do-not-reply@floss-pa.net>'

  def new_invite(community,invitation_sends)
    @community=community
    @invite=invitation_sends
    @user=invitation_sends.invite.user

      mail(to: @invite.email, subject: t(:group_invite,
         user_name: @user.name, group_name: @community.name))
  end
end
