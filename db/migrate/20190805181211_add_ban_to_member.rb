class AddBanToMember < ActiveRecord::Migration[5.1]
  def change
    add_column :members, :ban, :boolean
    add_column :members, :ban_by, :integer
    add_column :members, :removed_by, :integer
  end
end
