class AddRemovedToMembers < ActiveRecord::Migration[5.1]
  def change
    add_column :members, :removed, :boolean, default: false
    add_index :members, [:community_id, :user_id], unique: true
  end
end
