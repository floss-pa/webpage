class AddInviteEmailToEvent < ActiveRecord::Migration[5.1]
  def change
    add_column :events, :invite_emails, :boolean, default: false
    add_column :events, :invite_emails_at, :datetime
  end
end
