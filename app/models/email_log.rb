class EmailLog < ApplicationRecord
  belongs_to :user
  belongs_to :recipient, class_name: 'User'
  belongs_to :emailable, polymorphic: true
end
