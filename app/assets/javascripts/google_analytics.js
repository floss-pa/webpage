document.addEventListener('turbolinks:load', function(event) {
  if (typeof gtag === 'function') {
    gtag('config', 'UA-137871688-1', {
      'page_location': event.data.url
    })
  }
})
