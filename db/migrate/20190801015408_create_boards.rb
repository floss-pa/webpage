class CreateBoards < ActiveRecord::Migration[5.1]
  def change
    create_table :boards do |t|
      t.references :community, foreign_key: true
      t.references :user, foreign_key: true
      t.string :subject
      t.integer :views, default: 0
      t.boolean :pin
      t.boolean :deleted, default: false

      t.timestamps
    end
  end
end
